#include <limits>
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

struct Edge {
	int src, dest, weight;
};

struct Graph {
	int E, V;
	vector<Edge> edges;
};

struct Graph* create_graph(int E, int V) {
	struct Graph* G = (struct Graph*) malloc(sizeof (struct Graph));
	G->E = E;
	G->V = V;
	G->edges.resize(E);

	return G;
}

void bellman_ford(struct Graph* G, int source) {
	int V = G->V;
	int E = G->E;
	int dist[V];

	for (int i = 0; i < V; i++)
		dist[i] = numeric_limits<int>::max();
	dist[source] = 0;

	for (int i = 1; i < V; i++)
		for (int j = 0; j < E; j++) {
			int u = G->edges[j].src;
			int v = G->edges[j].dest;
			int w = G->edges[j].weight;
			if (dist[u] != numeric_limits<int>::max() && dist[v] > dist[u] + w)
				dist[v] = dist[u] + w;
		}

	for (int i = 0; i < E; i++) {
		int u = G->edges[i].src;
		int v = G->edges[i].dest;
		int w = G->edges[i].weight;
		if (dist[u] != numeric_limits<int>::max() && dist[v] > dist[u] + w) {
			cout << "Negative weight cycle found" << endl;
			return;
		}
	}

	cout << "Vertex    Distance from source" << endl;	
	for (int i = 0; i < V; i++)
		cout << i << "		" << dist[i] << endl;

	return;		
}



int main()
{
    /* Let us create the graph given in above example */
    int V = 5;  // Number of vertices in graph
    int E = 8;  // Number of edges in graph
    struct Graph* graph = create_graph(E, V);

    // add edge 0-1 (or A-B in above figure)
    graph->edges[0].src = 0;
    graph->edges[0].dest = 1;
    graph->edges[0].weight = -1;
 
    // add edge 0-2 (or A-C in above figure)
    graph->edges[1].src = 0;
    graph->edges[1].dest = 2;
    graph->edges[1].weight = 4;
 
    // add edge 1-2 (or B-C in above figure)
    graph->edges[2].src = 1;
    graph->edges[2].dest = 2;
    graph->edges[2].weight = 3;
 
    // add edge 1-3 (or B-D in above figure)
    graph->edges[3].src = 1;
    graph->edges[3].dest = 3;
    graph->edges[3].weight = 2;
 
    // add edge 1-4 (or A-E in above figure)
    graph->edges[4].src = 1;
    graph->edges[4].dest = 4;
    graph->edges[4].weight = 2;
 
    // add edge 3-2 (or D-C in above figure)
    graph->edges[5].src = 3;
    graph->edges[5].dest = 2;
    graph->edges[5].weight = 5;
 
    // add edge 3-1 (or D-B in above figure)
    graph->edges[6].src = 3;
    graph->edges[6].dest = 1;
    graph->edges[6].weight = 1;
 
    // add edge 4-3 (or E-D in above figure)
    graph->edges[7].src = 4;
    graph->edges[7].dest = 3;
    graph->edges[7].weight = -3;

    bellman_ford(graph, 0);
 
    return 0;
}