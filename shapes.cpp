#include <algorithm>
#include <iostream>
#include <vector>
#include <utility>

#define point pair<int, int>
#define side pair<point, point>

using namespace std;

bool vertical_cmp(point p1, point p2) {
	return (p1.second < p2.second);
}

bool parallel(side s1, side s2) {
	double factor1, factor2;
	if (s1.first.second == s1.second.second && s2.first.second == s2.second.second)
		return true;
	factor1 = (double) (s1.second.first - s1.first.first) / 
			  (double) (s1.second.second - s1.first.second);
	factor2 = (double) (s2.second.first - s2.first.first) / 
			  (double) (s2.second.second - s2.first.second);
	return (factor1 == factor2);
}

bool right_angle(side s1, side s2) {
	double factor1, factor2;
	if (s1.first.first == s1.second.first)
		return (s2.first.second == s2.second.second);
	if (s2.first.first == s2.second.first)
		return (s1.first.second == s1.second.second);
	factor1 = (double) (s1.second.first - s1.first.first) / 
			  (double) (s1.second.second - s1.first.second);
	factor2 = (double) (s2.second.first - s2.first.first) / 
			  (double) (s2.second.second - s2.first.second);
	return (factor1 * factor2 == -1.0);
}

bool equal_len(side s1, side s2) {
	int d1x = s1.second.first - s1.first.first;
	int d1y = s1.second.second - s1.first.second;
	int d2x = s2.second.first - s2.first.first;
	int d2y = s2.second.second - s2.first.second;

	return (d1x * d1x + d1y * d1y == d2x * d2x + d2y * d2y);
}

int main() {
	int t;
	point p1, p2, p3, p4;
	vector<point> vert, hor;
	vector<side> sides;

	cin >> t;
	for (int i = 0; i < t; i++) {
		vector<point> vert, hor;
		vector<side> sides;
		vert.resize(4);
		hor.resize(4);
		sides.resize(4);
		cin >> p1.first >> p1.second;
		cin >> p2.first >> p2.second;
		cin >> p3.first >> p3.second;
		cin >> p4.first >> p4.second;
		vert[0] = p1;
		vert[1] = p2;
		vert[2] = p3;
		vert[3] = p4;
		hor = vert;

		sort(hor.begin(), hor.end());
		sort(vert.begin(), vert.end(), vertical_cmp);
		sides[0].first = vert[0];
		sides[0].second = vert[1];
		if (sides[0].first.first > sides[0].second.first)
			swap(sides[0].first, sides[0].second);
		sides[1].first = hor[2];
		sides[1].second = hor[3];
		if (sides[1].first.first > sides[1].second.first)
			swap(sides[1].first, sides[1].second);
		sides[2].first = vert[2];
		sides[2].second = vert[3];
		if (sides[2].first.first > sides[2].second.first)
			swap(sides[2].first, sides[2].second);
		sides[3].first = hor[0];
		sides[3].second = hor[1];
		if (sides[3].first.first > sides[3].second.first)
			swap(sides[3].first, sides[3].second);

		for (int j = 0; j != 4; j++) {
			cout << sides[j].first.first << " " << sides[j].first.second << endl;
		}

		if (!parallel(sides[0], sides[2]) && !parallel(sides[1], sides[3])) {
			cout << "case " << i+1 << ": Ordinary Quadrilateral" << endl;
			break;
		}
		if (!(parallel(sides[0], sides[2]) && parallel(sides[1], sides[3]))) {
			cout << "case " << i+1 << ": Trapezium" << endl;
			break;
		}
		if (right_angle(sides[0], sides[1])) {
			if (equal_len(sides[0], sides[1])) {
				cout << "case " << i+1 << ": Square" << endl;
				break;	
			}
			else {
				cout << "case " << i+1 << ": Rectangle" << endl;
				break;
			}
		}
		else {
			if (equal_len(sides[0], sides[1])) {
				cout << "case " << i+1 << ": Rhombus" << endl;
				break;	
			}
			else {
				cout << "case " << i+1 << ": Parallelogram" << endl;
				break;
			}	
		}
	}

	return 0;
}