#include <algorithm>
#include <iostream>
#include <vector>
#include <utility>

using namespace std;

struct monster {
	int dmg, potion, id;
};

bool potion_compare(monster m1, monster m2) {
	if (m1.potion <= m2.potion)
		return true;
	return false;
}

bool dmg_compare(monster m1, monster m2) {
	if (m1.dmg <= m2.dmg)
		return true;
	return false;
}

int main() 
{
	int n, z;
	struct monster temp;
	vector<struct monster> healthy_monsters, monsters;
	vector<int> order;
	cin >> n >> z;
	for (int i = 0; i < n; i++) {
		cin >> temp.dmg >> temp.potion;
		temp.id = i;	
		if (temp.dmg <= temp.potion)
			healthy_monsters.push_back(temp);
		else
			monsters.push_back(temp);
	}

	sort(healthy_monsters.begin(), healthy_monsters.end(), dmg_compare);
	sort(monsters.begin(), monsters.end(), potion_compare);

	for (vector<pair<int, int> >::const_iterator it = healthy_monsters.begin();
		it != healthy_monsters.end(); it++) {
		if (it->dmg > z) {
			cout << "NIE";
			return 0;
		}
		else {
			z += (it->potion - it->dmg);
			order.push_back(it->id)
		}
	}

	for (vector<pair<int, int> >::const_iterator it = monsters.begin();
		it != monsters.end(); it++) {
		if (it->dmg > z) {
			cout << "NIE";
			return 0;
		}
		else {
			z += (it->potion - it->dmg);
			order.push_back(it->id);
		}
	}
	
	cout << "TAK" << endl;
	cout << order[0]+1;
	for (int i = 1; i != order.size(); i++)
		cout << " " << order[i]+1;

	return 0;
}