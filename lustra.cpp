#include <iostream>
#include <limits>
#include <vector>

using namespace std;

struct firma {
	int x_min, x_max, y_min, y_max;
};

int main() {
	int t, n, kand;
	struct firma temp;

	cin >> t;
	for (int i = 0; i < t; i++) {
		if (i > 0)
			cout << endl;
		cin >> n;
		int x_min = numeric_limits<int>::max(); 
		int x_max = 0;
		int y_min = numeric_limits<int>::max();
		int y_max = 0;
		kand = -1;
		for (int j = 0; j < n; j++) {
			cin >> temp.x_min;
			if (temp.x_min < x_min) {
				x_min = temp.x_min;
				kand = -1;
			}
			cin >> temp.x_max;
			if (temp.x_max > x_max) {
				x_max = temp.x_max;
				kand = -1;
			}
			cin >> temp.y_min;
			if (temp.y_min < y_min) {
				y_min = temp.y_min;
				kand = -1;
			}
			
			cin >> temp.y_max;
			if (temp.y_max > y_max) {
				y_max = temp.y_max;
				kand = -1;
			}

			if (temp.x_min == x_min && temp.x_max == x_max &&
				temp.y_min == y_min && temp.y_max == y_max)
				kand = j;
		}
		if (kand >= 0)
			cout << "TAK";
		else
			cout << "NIE";
	}
}